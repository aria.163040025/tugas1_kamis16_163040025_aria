<?php 


class Teknologi extends Controller {

  public function index() {
    if (isset($_POST["cari"])) {
      $data = array(
        'judul' => "Daftar Perangkat Teknologi",
        'tek' => $this->model('Teknologi_model')->cariTeknologi($_POST['keyword'])
      );
      $this->view('templates/header', $data);
      $this->view('teknologi/index', $data);
      $this->view('templates/footer');
    } else {
      $data = array(
        'judul' => "Daftar Perangkat Teknologi",
        'tek' => $this->model('Teknologi_model')->getAllTeknologi()
      );
      $this->view('templates/header', $data);
      $this->view('teknologi/index', $data);
      $this->view('templates/footer');
    }
  }

  public function detail($id) {
    $data = array(
      'judul' => "Daftar Teknologi",
      'tek' => $this->model('Teknologi_model')->getTeknologiById($id)
    );
    
    $this->view('templates/header', $data);
    $this->view('teknologi/detail', $data);
    $this->view('templates/footer');
    }

    public function insertTeknologi() {
      if (isset($_POST["submit"])) {
        $model = $this->model('Teknologi_model');
      $data['nama'] = $_POST['nama'];
      $data['produsen'] = $_POST['produsen'];
      $data['spek'] = $_POST['spek'];
      $data['tahun'] = $_POST['tahun'];
      $model->insertTeknologi($data);
      header("Location: ../Teknologi");
    } else {
        $model = $this->model('Teknologi_model');
        $data = [
          'judul' => "Tambah Teknologi",
          'tek' => $model->getAllTeknologi()
        ];
        $this->view('templates/header', $data);
        $this->view('teknologi/tambah', $data);
        $this->view('templates/footer');
    }
  }

  public function editTeknologi($id) {
    if (isset($_POST["submit"])) {
      $model = $this->model('Teknologi_model');
      $data['id'] = $_POST['id'];
      $data['nama'] = $_POST['nama'];
      $data['produsen'] = $_POST['produsen'];
      $data['spek'] = $_POST['spek'];
      $data['tahun'] = $_POST['tahun'];
      $model->editTeknologi($data);
      header("Location: ../Teknologi");
    } else {
        $model = $this->model('Teknologi_model');
        $data = [
          'judul' => "Tambah Teknologi",
          'tek' => $model->getTeknologiById($id),
        ];
      $this->view('templates/header', $data);
      $this->view('teknologi/edit', $data);
      $this->view('templates/footer');                  
    }
  }

    public function hapusTeknologi($id) {
      $model = $this->model('Teknologi_model');
      $model->hapusTeknologi($id);
      header("Location: ../Teknologi");
    }

    public function cariTeknologi($data) {
      if (isset($_POST["submit"])) {
        $model = $this->model('Teknologi_model');
        $data['keyword'] = $_POST['keyword'];
        $model->cariTeknologi($data);             
      } else {
        $model = $this->model('Teknologi_model');
        $data = [
          'judul' => "Cari Teknologi",
          'tek' => $model->cariTeknologi($data)
        ];
        $this->view('templates/header', $data);
        $this->view('teknologi/detail', $data);
        $this->view('templates/footer');
      }
    }
  }
