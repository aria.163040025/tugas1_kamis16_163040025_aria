<?php 


class About extends Controller {

  public function index($nama = "Aria", $job = "Kapiten" , $umur = "21") {
    $data = [
      'nama' => $nama,
      'job' => $job,
      'umur' => $umur,
      'judul' => 'Tentang Kami'
    ];

    $this->view('templates/header', $data);
    $this->view('about/index', $data);
    $this->view('templates/footer');
  }
  
public function page() {
    $data['judul'] = "Pages";
    $this->view('templates/header', $data);
    $this->view('about/page');
    $this->view('templates/footer');   
  }
}

?>
