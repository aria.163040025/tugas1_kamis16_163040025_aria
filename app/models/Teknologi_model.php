<?php 


class Teknologi_model {
  private $table = 'teknologi';
  private $db;
  private $name;
  private $jurusan;
  private $nrp;
  private $email;

  public function __construct() {
    $this->db = new Database;
  }

  public function getAllTeknologi() {
    $this->db->query("SELECT * FROM " . $this->table);
    return $this->db->resultSet();
  }

  public function getTeknologiByID($id) {
    $this->db->query("SELECT * FROM " . $this->table . " WHERE id=:id");
    $this->db->bind("id", $id);
    return $this->db->single();
  }

  public function insertTeknologi($data) {
    $nama = $data['nama'];
    $produsen = $data['produsen'];
    $spek = $data['spek'];
    $tahun = $data['tahun'];

    $query = "INSERT INTO $this->table VALUES ('', '$nama', '$produsen', '$spek', '$tahun')";
    $this->db->query($query);
    $this->db->execute();

  }

  public function editTeknologi($data) {
    $id = $data['id'];
    $nama = $data['nama'];
    $produsen = $data['produsen'];
    $spek = $data['spek'];
    $tahun = $data['tahun'];

    $query = "UPDATE $this->table SET nama = '$nama', produsen = '$produsen', spek = '$spek', tahun = '$tahun' WHERE id = '$id'";
    $this->db->query($query);
    $this->db->execute();  
  }

  public function hapusTeknologi($id) {
    $query = "DELETE FROM $this->table WHERE id='$id'";
    $this->db->query($query);
    $this->db->execute();
}

  public function cariTeknologi($data) {
    $keyword = $data;
    $query = "SELECT * FROM " . $this->table . " WHERE nama LIKE '%$keyword%' OR produsen LIKE '%$keyword%' OR spek LIKE '%$keyword%' OR tahun LIKE '%$keyword%'";
    $this->db->query($query);
    return $this->db->resultSet();
  }
}




?>
