<br><br>
<div class="container mt-5">
   <div class="container mt-5">
      <h3 style="text-align: center">Edit Produk</h3><br>
      <form action="<?= BASEURL; ?>/teknologi/editTeknologi/$data['tek]['id']" method="post">
         <div class="for-group col-4" style="margin: auto">
            <input type="hidden" name="id" value="<?= $data['tek']['id'] ?>">
         </div>
         <div class="form-group col-4" style="margin: auto">
            <label>Nama:</label>
            <input type="text" class="form-control" name="nama" value="<?= $data['tek']['nama'] ?>">
         </div><br>
         <div class="form-group col-4" style="margin: auto">
            <label>Produsen:</label>
            <input type="text" class="form-control" name="produsen" value="<?= $data['tek']['produsen'] ?>" name="produsen">
         </div><br>
         <div class="form-group col-4" style="margin: auto">
            <label>Spesifikasi:</label>
            <input type="text" class="form-control" value="<?= $data['tek']['spek'] ?>" name="spek">
         </div><br>
         <div class="form-group col-4" style="margin: auto">
            <label>Tahun:</label>
            <input type="text" class="form-control" value="<?= $data['tek']['tahun'] ?>" name="tahun">
         </div><br><br>
         <center><button type="submit" id="submit" name="submit" class="btn btn-primary">Edit Produk</button></center>
      </form>
   </div>
</div>
