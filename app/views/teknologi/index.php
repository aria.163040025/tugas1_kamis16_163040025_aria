<br><br>
<div class="container mt-5">
  <div class="row">
    <div class="col-7" style="margin: auto;">
    <h3 style="text-align: center">Daftar Mahasiswa</h3><br>
    <form action="<?= BASEURL; ?>/teknologi/" method="post">
      <center>
        <input type="text" name="keyword">
        <button class="btn-sm btn-dark" type="submit" name="cari" style="font-size: 15px;">Cari Produk</button>
      </center>
    </form>
    <br><br>
    <center><a href="<?= BASEURL; ?>/teknologi/insertTeknologi" style="text-decoration: none;" class="btn-success btn-lg">Tambah Produk</a ></center>
    <br>
    <?php foreach ($data['tek'] as $key  ) { ?>
      <ul class="list-group">
        <li class="list-group-item d-flex justify-content-between align-items-center" style="box-shadow: 0 1px 2px grey">
          <span style="width: 300px; font-size: 20px"><?= $key['nama']?></span>
          <a href="<?= BASEURL; ?>/teknologi/detail/<?=$key['id']; ?>" class="badge" style="font-size: 16px; line-height: 30px; background-color:rgb(244, 81, 30)">Detail</a>
          <a href="<?= BASEURL; ?>/teknologi/editTeknologi/<?=$key['id']; ?>" class="badge badge-warning" style="font-size: 16px; line-height: 30px; background-color:#0CB1EE">Edit</a>
          <a href="<?= BASEURL; ?>/teknologi/hapusTeknologi/<?=$key['id']; ?>" class="badge badge-danger" onclick = 'return confirm("Apakah anda yakin?")' style="font-size: 16px; line-height: 30px; background-color: #FF5959">Delete</a>
        </li>
      </ul>
    <?php } ?>   
  </div>
</div>
<br><br>
