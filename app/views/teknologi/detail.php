<br><br>
<div class="container">
   <div class="card" style="width: 18rem; margin: 0 auto"><br><br>
      <div class="card-body">
         <h2 class="card-title text-center"><?= $data['tek']['nama'] ?></h2>
         <center>Produsen:</center>
         <h2 class="card-subtitle mb-2 text-muted text-center"><?= $data['tek']['produsen'] ?></h2>
         <center>Spesifikasi:</center>
         <p class="card-text text-center"><?= $data['tek']['spek'] ?></p>
         <center>Tahun:</center>
         <p class="card-text text-center"><?= $data['tek']['tahun'] ?></p>
         <center><a href="<?= BASEURL ; ?>/teknologi" class="card-link">Kembali</a></center>
      </div>
   </div>
</div>
<br><br><br><br><br><br><br>
