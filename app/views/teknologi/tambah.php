<br><br>
  <div class="container mt-5">
  <h3 style="text-align: center">Tambah Produk</h3><br>
  <form action="<?= BASEURL; ?>/teknologi/insertTeknologi" method="post">
    <div class="form-group col-4" style="margin: auto">
      <label>Nama:</label>
      <input type="text" class="form-control" placeholder="Masukkan nama" name="nama">
    </div><br>
    <div class="form-group col-4" style="margin: auto">
      <label>Produsen:</label>
      <input type="text" class="form-control" placeholder="Masukkan produsen" name="produsen">
    </div><br>
    <div class="form-group col-4" style="margin: auto">
      <label>Spek:</label>
      <input type="text" class="form-control" placeholder="Masukkan Spesifikasi" name="spek">
    </div><br>
    <div class="form-group col-4" style="margin: auto">
      <label>Tahun:</label>
      <input type="text" class="form-control" placeholder="Masukkan tahun" name="tahun">
    </div><br><br>
    <center>
      <button type="submit" id="submit" name="submit" class="btn btn-primary">Tambah</button>
    </center>
  </form>
</div>
