<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60" style="background-image: url('<?php echo BASEURL; ?>/img/qbkls.png')">

<div class="jumbotron text-center" style="box-shadow: 0px 5px 10px gray;">
  <h1>Selamat Datang di Website Kami!</h1>
  <p>Serba-Serbi Teknologi!</p> 
</div>

<!-- Container (About Section) -->
<div id="about" class="container-fluid">
  <div class="row">
    <div class="col-sm-15" style="text-align: center">
      <h2>Tentang Kami</h2><br>
      <h4 style="line-height: 40px">Website ini berisi beberapa produk perangkat keras yang berhubungan dengan komputer. <br>Dan website ini dibuat untuk memenuhi Tugas Mata Praktikum <b>Rekayasa Web</b> <br> <b>Teknik Informatika</b> <br> <b>Universitas Pasundan</b></h4><br>
      <p>Berikut adalah anggota dibalik website ini:</p>
      <ul>
        <br>Aria Bisma Wahyutama - 163040025
        <br>Iqyan Sufyan - 163040017
        <br>M. Farhan Prawiradinata - 143040225
      </ul>
    </div>
  </div>
</div>
